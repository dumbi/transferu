from pony.orm import *
from datetime import datetime
from Entities import Record, House, db
from pwhash import *

class DbContext:
    def __init__(self):
        self.__format = '%Y/%m/%d'
        db.bind('sqlite', 'test40.db', create_db=True)
        db.generate_mapping(create_tables=True)
        self.PopulateDB()

    @db_session
    def PopulateDB(self):
        record1 = Record(Consumption=1666, Date=datetime.strptime("2019/11/16", self.__format))
        record2 = Record(Consumption=2033, Date=datetime.strptime("2019/11/17", self.__format))
        record3 = Record(Consumption=7562, Date=datetime.strptime("2019/11/18", self.__format))
        record4 = Record(Consumption=2645, Date=datetime.strptime("2019/11/19", self.__format))
        record5 = Record(Consumption=4500, Date=datetime.strptime("2019/11/20", self.__format))

        house1 = House(Type=0, Area=70, Rooms=3, HeatingType='fuel', YearOfConstruction=1908,
                       StreetNumber=1, StreetName="impasse des Lilas", Postal="10220",
                       City="Assencières",Record=[record1, record3],
                       Username="dupontbernard", Password=hash_password("bFUhPDJhSRV22"),
                       Association="Société HLM de l’Aude (SHA)", Activated=True)

    @db_session
    def getAllRecordByHouseID(self, id):
        with db_session:
            return select(h.Record for h in House if h.HouseID == id).fetch()

    @db_session
    def getAll(self, entity):
        return select(h for h in entity).fetch()

    @db_session
    def getUserByName(self, name):
        return select(u for u in House if u.Username == name).first()

    @db_session
    def getUserByToken(self, token):
        return select(u for u in House if u.Token == token).first()

    @db_session
    def getAllRecordByToken(self, token):
        return select(h.Record for h in House if h.Token == token).fetch()

    @db_session
    def updateToken(self, name, token):
        user = self.getUserByName(name)
        user.Token = token
        db.commit()

    @db_session
    def DeleteToken(self, token):
        user = self.getUserByToken(token)
        if user:
            user.Token = ""
            db.commit()

    @db_session
    def UpdatePassword(self, name, passw):
        user = self.getUserByName(name)
        user.Password = passw
        db.commit()

    @db_session
    def RegisterUser(self, type, area, rooms, heatingtype, year, streetnum, streetname, postalcode, city, username, password, fname, sname, Association="", Activation=True):
        House(Type=type, Area=area, Rooms=rooms, HeatingType=heatingtype, YearOfConstruction=year,
              StreetNumber=streetnum, StreetName=streetname, Postal=postalcode,
              City=city, Username=username, Password=hash_password(password),
              Association=Association, Activated=Activation)

    @db_session
    def AddRecord(self, token, consumption, date):
        user = self.getUserByToken(token)
        start = datetime.now()
        if user:
            record = Record(Consumption=consumption, Date=datetime.strptime(f"{start.year}/{start.month}/{start.day}", self.__format))
            user.Record.add(record)
            db.commit()

