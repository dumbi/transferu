import os
import uuid
from py_linq import Enumerable
from pwhash import *
from flask_mail import Mail, Message
from datetime import timedelta, datetime
from DbContext import DbContext
from flask import Flask, render_template, url_for, request, flash, redirect, make_response, session


app = Flask(__name__)
app.config['SECRET_KEY'] = '9OLWxND4o83j4K4iuopO'
app.config['PERMANENT_SESSION_LIFETIME'] = timedelta(seconds=5)
mail_settings = {
    "MAIL_SERVER": 'smtp.gmail.com',
    "MAIL_PORT": 587,
    "MAIL_USE_TLS": True,
    "MAIL_USE_SSL": False,
    "MAIL_USERNAME": os.environ['EMAIL_USER'],
    "MAIL_PASSWORD": os.environ['EMAIL_PASSWORD']
}

app.config.update(mail_settings)
mail = Mail(app)
activeResetTokens = {}

print(os.environ['EMAIL_USER'])
print(os.environ['EMAIL_PASSWORD'])

ctx = DbContext()

def GenerateToken():
    return uuid.uuid4().hex

def isLoggedIn():
    if "token" not in request.cookies:
        return False
    else:
        if request.cookies.get("token") != "" and ctx.getUserByToken(request.cookies.get("token") ):
            return True
        return False

def GenerateRecordString(start, records):
    result = ""
    dateformat = '%Y/%m/%d'
    start = datetime.strptime(f"{start.year}/{start.month}/{start.day}", dateformat)
    for i in range(30):
        print(start)
        record = Enumerable(records).where(lambda x: x.Date == start).first_or_default()
        if record:
            result = str(record.Consumption) + ';' + result
        else:
            result = '0;' + result
        start += timedelta(days=-1)
    return result

def sendEmail(link, email):
    with app.app_context():
        msg = Message(subject="Reset your Password",
                      sender=app.config.get("MAIL_USERNAME"),
                      recipients=[email], # replace with your email for testing
                      body="Hello!\n"
                           "This is your reset link\n\n"
                           f"{link}\n\n"
                           "Best regards,\n"
                           "Meter")
        mail.send(msg)

@app.route('/', methods=["POST", "GET"])
def index():
    if isLoggedIn():
        dateformat = '%Y/%m/%d'
        token = request.cookies.get("token")
        dateinit = str(request.form.get("date")).replace('-', '/').replace(' ', '')
        dateinit = datetime.strptime(dateinit, dateformat)
        user = ctx.getUserByToken(token)
        records = ctx.getAllRecordByHouseID(user.HouseID)
        return render_template("charts-v9-USER.html",
                               records=GenerateRecordString(dateinit, records),
                               startdate=f"{dateinit.year}-{dateinit.month}-{dateinit.day}")
    return redirect(url_for('login'))


@app.route('/dashboard')
def dashboard():
    if isLoggedIn():
        token = request.cookies.get("token")
        dateinit = (datetime.now() + timedelta(days=-29))
        user = ctx.getUserByToken(token)
        records = ctx.getAllRecordByHouseID(user.HouseID)
        return render_template("charts-v9-USER.html",
                               records=GenerateRecordString(datetime.now(), records),
                               startdate=f"{dateinit.year}-{dateinit.month}-{dateinit.day}")
    return redirect(url_for('login'))


@app.route('/login')
def login():
    return render_template('login.html')


@app.route('/reset/<string:token>')
def resetpass(token):
        return render_template('reset.html',
                               token=token)


@app.route('/reset', methods=["POST"])
def resetcheckpass():
    token = request.form.get("token")
    passw = request.form.get('password')
    passagain = request.form.get('passwordagain')
    if token in activeResetTokens:
        if passagain == passw:
            if datetime.now() - activeResetTokens[token][1] < timedelta(minutes=5):

                username = activeResetTokens[token][0]
                ctx.UpdatePassword(username, hash_password(passw))
                del activeResetTokens[token]
                flash('Password successfully changed!')
                return redirect(url_for('login'))
            else:
                flash('This link is not valid!')
                return redirect(url_for('login'))
        else:
            flash('The passwords do not match')
            return render_template('reset.html',
                                   token=token)
    flash('This link is not valid!')
    return redirect(url_for('login'))

@app.route('/resetrequest')
def resetrquest():
    return render_template("resetrequest.html")

@app.route('/resetrequest', methods=["POST"])
def SendoutMail():
    email = request.form.get("email")
    user = ctx.getUserByName(email)
    if user:
        token = str(GenerateToken())
        activeResetTokens[token] = (email, datetime.now())
        sendEmail(f"http://localhost:5000/reset/{token}", email)
        flash('We have sent an email with the instructions!')
        return redirect(url_for('login')) # if user doesn't exist or password is wrong, reload the page
    else:
        flash('Email was not found!')
        return redirect(url_for('resetrquest'))

@app.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = ctx.getUserByName(email)

    # check if user actually exists
    # take the user supplied password, hash it, and compare it to the hashed password in database
    if not user or not verify_password(user.Password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('login')) # if user doesn't exist or password is wrong, reload the page

    session.permanent = True
    # if the above check passes, then we know the user has the right credentials
    if "token" not in request.cookies:
        print("token added")
        token = str(GenerateToken())
        ctx.updateToken(email, token)
        resp = make_response(redirect(url_for('dashboard')))
        resp.set_cookie('token', token)
        return resp
    return redirect(url_for('dashboard'))


@app.route('/logout')
def logout():
    ctx.DeleteToken(request.cookies.get("token"))
    resp = make_response(redirect(url_for('login')))
    resp.set_cookie('token', '', expires=0)
    return resp

@app.route('/signup')
def signup():
    return render_template('signup.html')

@app.route('/signup', methods=["POST"])
def signup_post():
    email = request.form.get('email')
    print(email)
    fname = request.form.get('fname')
    sname = request.form.get('sname')
    passw = request.form.get('password')
    passwagn = request.form.get('passwordagain')
    housetype = request.form.get('type')
    year = request.form.get('year')
    area = request.form.get('area')
    numofrooms = request.form.get('rooms')
    fuel = request.form.get('heating')
    postal = request.form.get('postal')
    city = request.form.get('city')
    street = request.form.get('streetname')
    streetnum = request.form.get('streetnum')
    association = request.form.get('association')
    if passw == passwagn:
        # ctx.RegisterUser(housetype, area, numofrooms, fuel, year, streetnum, street, postal, city, email, passw, fname, sname, association,)
        flash('Succesful registration!')
        return redirect(url_for('login'))
    flash('Passwords do not match')
    return render_template("signup.html")

if __name__ == '__main__':
    app.run()
