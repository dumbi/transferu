from pony.orm import *
from datetime import datetime

db = Database()

class House(db.Entity):
    HouseID = PrimaryKey(int, auto=True)
    Type = Required(int)
    Area = Required(int)
    Rooms = Required(int)
    HeatingType = Required(str)
    YearOfConstruction = Required(int)
    StreetNumber = Required(int)
    StreetName = Required(str)
    Postal = Required(str)
    City = Required(str)
    Record = Set("Record")
    Username = Required(str)
    Password = Required(str)
    Fname = Optional(str)
    Sname = Optional(str)
    Association = Optional(str)
    Token = Optional(str)
    Activated = Required(bool)


class Record(db.Entity):
    RecordID = PrimaryKey(int, auto=True)
    House = Optional(House)
    Consumption = Required(int)
    Date = Required(datetime)